import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import {
  addQuantity,
  removeItem,
  subtractQuantity,
} from './actions/cartActions'

export const Cart = props => {
  const { items, removeItem, addQuantity, subtractQuantity } = props

  const handleRemove = id => {
    removeItem(id)
  }

  const handleAddQuantity = id => {
    addQuantity(id)
  }

  const handleSubtractQuantity = id => {
    subtractQuantity(id)
  }

  const addedItems = items?.length ? (
    items.map(item => {
      // Implement the cart list here
      return <></>
    })
  ) : (
    <p>Nothing.</p>
  )
  return (
    <div className="container">
      <div className="cart">
        <h5>You have ordered:</h5>
        {addedItems}
      </div>
    </div>
  )
}

const mapStateToProps = state => null

const mapDispatchToProps = dispatch => ({
  removeItem: id => {
    dispatch(removeItem(id))
  },
  addQuantity: id => {
    dispatch(addQuantity(id))
  },
  subtractQuantity: id => {
    dispatch(subtractQuantity(id))
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(Cart)
